package test.java;

import org.junit.Assert;
import org.junit.Test;

import main.java.Calculo;

public class CalculoTest {

	@Test
	public void verificarFatorial(){
		Calculo calculo = new Calculo();
		Assert.assertEquals(Integer.valueOf(5040), calculo.fatorial(7));
	}
}
