package main.java;

public class Calculo {

	public Integer fatorial(Integer num) {
		if (num <= 1) {
			return 1;
		} else {
			return fatorial(num - 1) * num;
		}
	}

}
